webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".container {\n height:100%;\n background-color:rgb(247, 244, 244)\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div class=\"containe\">\n  <div class=\"row header\">\n    <div class=\"col-md-12 \">\n      <router-outlet></router-outlet>\n    </div>\n  </div>\n \n  \n<!--<div class=\"container\" *ngIf=\"userService.loggedIn\">\n  <div class=\"row center\">\n    <div class=\"col-md-2 leftSidebar\">\n      <app-left-sidebar></app-left-sidebar>\n    </div>\n    <div class=\"col-md-7 content\">\n     <app-content></app-content>\n    </div>\n    <div class=\"col-md-2 rightSidebar\">\n      <app-right-sidebar></app-right-sidebar>\n    </div>\n  </div>\n \n  <div class=\"row footer\">\n    <div class=\"col-md-12\">\n      <app-footer></app-footer>\n    </div>\n  </div> \n</div> -->\n</div>\n  \n\n\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_service__ = __webpack_require__("../../../../../src/app/services/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(userService) {
        this.userService = userService;
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_socket_io__ = __webpack_require__("../../../../ng-socket-io/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_activity_service__ = __webpack_require__("../../../../../src/app/services/activity.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_user_service__ = __webpack_require__("../../../../../src/app/services/user.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_header_header_component__ = __webpack_require__("../../../../../src/app/components/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_content_content_component__ = __webpack_require__("../../../../../src/app/components/content/content.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_left_sidebar_left_sidebar_component__ = __webpack_require__("../../../../../src/app/components/left-sidebar/left-sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_right_sidebar_right_sidebar_component__ = __webpack_require__("../../../../../src/app/components/right-sidebar/right-sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_footer_footer_component__ = __webpack_require__("../../../../../src/app/components/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_register_register_component__ = __webpack_require__("../../../../../src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_user_register_user_register_component__ = __webpack_require__("../../../../../src/app/components/user-register/user-register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_device_reg_device_reg_component__ = __webpack_require__("../../../../../src/app/components/device-reg/device-reg.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var server_url = 'http://45.55.167.142:3001';
//const server_url:string = 'http://localhost:3001';

var config = { url: server_url, options: {} };
//const config: SocketIoConfig = { url: 'http://localhost:3001', options: {} };
//custom dependencies













//routes
var routes = [
    {
        path: "", redirectTo: "admin/login", pathMatch: "full"
    },
    {
        path: "admin", redirectTo: "admin/login", pathMatch: "full"
    },
    {
        path: "admin/login", component: __WEBPACK_IMPORTED_MODULE_14__components_login_login_component__["a" /* LoginComponent */]
    },
    {
        path: "admin/register", component: __WEBPACK_IMPORTED_MODULE_15__components_register_register_component__["a" /* RegisterComponent */]
    },
    {
        path: "admin/dashboard", component: __WEBPACK_IMPORTED_MODULE_16__components_dashboard_dashboard_component__["a" /* DashboardComponent */]
    },
    {
        path: "admin/device/register", component: __WEBPACK_IMPORTED_MODULE_18__components_device_reg_device_reg_component__["a" /* DeviceRegComponent */]
    }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_header_header_component__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_content_content_component__["a" /* ContentComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_left_sidebar_left_sidebar_component__["a" /* LeftSidebarComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_right_sidebar_right_sidebar_component__["a" /* RightSidebarComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_15__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_16__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_18__components_device_reg_device_reg_component__["a" /* DeviceRegComponent */],
                __WEBPACK_IMPORTED_MODULE_17__components_user_register_user_register_component__["a" /* UserRegisterComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["c" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_5_ng_socket_io__["SocketIoModule"].forRoot(config),
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* RouterModule */].forRoot(routes)
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__services_activity_service__["a" /* Activity */],
                __WEBPACK_IMPORTED_MODULE_7__services_user_service__["a" /* UserService */],
                {
                    provide: "API_SERVER_URL", useValue: server_url
                }
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/content/content.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".content{\n width:70%;\n height:100%;\n position:fixed;\n left:15%;\n top:15%;\n background-color:rgb(231, 223, 223);\n overflow:scroll;\n}\n\ntable {\n    width:100%;\n   \n}\n\n.form{\n    width:400px;\n    margin-top:10%;margin-bottom:20%;\n    margin-left:20%;margin-right:20%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/content/content.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content\">\n  <table class=\"table\" *ngIf=\"which === 'pushes'\">\n    <tr>\n      <td>\n        S/N\n      </td>\n      <td>\n        Device ID\n      </td>\n      <td>\n        Latitude\n      </td>\n      <td>\n        Longitude\n      </td>\n      <td>\n        Time\n      </td>\n      <td>\n        Status\n      </td>\n    </tr>\n    \n    <tr *ngFor=\"let push of activity.pushList; let i = index;\">\n      <td>\n        {{i+1}} \n      </td>\n      <td>\n        {{push.deviceID}}\n      </td>\n      <td>\n        {{push.lat}}\n      </td>\n      <td>\n        {{push.lng}}\n      </td>\n      <td>\n        {{push.time}}\n      </td>\n      <td>\n        {{push.status}}\n      </td>\n    </tr> \n  </table>\n\n\n  <table class=\"table\" *ngIf=\"which === 'users'\">\n      <tr>\n        <td>\n          S/N\n        </td>\n        <td>\n          User Name\n        </td>\n        <td>\n          Full Name\n        </td>\n        <td>\n          Gender\n        </td>\n        <td>\n          Phone\n        </td>\n        <td>\n          Email\n        </td>\n      </tr>\n      \n      <tr *ngFor=\"let user of activity.userList; let i = index;\">\n        <td>\n          {{i+1}} \n        </td>\n        <td>\n          {{user.username}}\n        </td>\n        <td>\n          {{user.fullname}}\n        </td>\n        <td>\n          {{user.gender}}\n        </td>\n        <td>\n          {{user.phone}}\n        </td>\n        <td>\n          {{user.email}}\n        </td>\n      </tr> \n    </table>\n\n\n    <table class=\"table\" *ngIf=\"which === 'list admin'\">\n        <tr>\n          <td>\n            S/N\n          </td>\n          <td>\n            Admin User Name\n          </td>\n          <td>\n            Email Address\n          </td>\n          <td>\n              Date Created\n            </td>\n        </tr>\n        \n        <tr *ngFor=\"let admin of activity.adminList; let i = index;\">\n          <td>\n            {{i+1}} \n          </td>\n          <td>\n            {{admin.username}}\n          </td>\n          <td>\n            {{admin.email}}\n          </td>\n          <td>\n              {{admin.createdAt}}\n            </td>\n        </tr> \n      </table>\n\n\n\n    <table class=\"table\" *ngIf=\"which === 'devices'\">\n        <tr>\n          <td>\n            S/N\n          </td>\n          <td>\n            Device ID\n          </td>\n          <td>\n            Device Name\n          </td>\n          <td>\n            Date Created\n          </td>\n          <td>\n            Device owner\n          </td>\n          <td>\n            Activated\n          </td>\n        </tr>\n        \n        <tr *ngFor=\"let device of activity.deviceList; let i = index;\">\n          <td>\n            {{i+1}} \n          </td>\n          <td>\n            {{device.deviceID}}\n          </td>\n          <td>\n            {{device.deviceName}}\n          </td>\n          <td>\n            {{device.createdAt}}\n          </td>\n          <td>\n            {{device.username}}\n          </td>\n          <td>\n            {{device.activated}}\n          </td>\n        </tr> \n      </table>\n\n\n      <div class = \"form\" *ngIf=\"which === 'add device'\">\n        <h3>Add New Device</h3>\n          <form role=\"form\" [formGroup] = 'deviceForm' (ngSubmit)=\"addDevice(deviceForm)\">\n              <div class=\"form-group\">\n                <label for=\"deviceID\">Device ID</label>\n                <input type=\"text\" class=\"form-control\" id = \"deviceID\" placeholder=\"Device ID\" [formControl] = 'deviceForm.controls[\"deviceID\"]'>\n              </div>\n              <div class=\"form-group\">\n                  <label for=\"deviceName\">Device Name</label>\n                  <input type=\"text\" class=\"form-control\" id = \"deviceID\" placeholder=\"Device ID\" [formControl] = 'deviceForm.controls[\"deviceName\"]'>\n              </div>\n              <div class=\"form-group\">\n                  <button class=\"btn btn-danger\" type=\"submit\">Add Device</button>\n              </div>\n          </form>\n      </div>\n\n      <div class=\"container\">\n          <app-user-register *ngIf=\"which === 'add user'\"></app-user-register>\n          <app-device-reg *ngIf=\"which === 'register device'\"></app-device-reg>\n          <app-register *ngIf=\"which === 'add admin'\"></app-register>\n      </div>\n</div>\n\n\n\n\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/components/content/content.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_activity_service__ = __webpack_require__("../../../../../src/app/services/activity.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var ContentComponent = (function () {
    function ContentComponent(activity, fb, http, api_url, router) {
        this.activity = activity;
        this.fb = fb;
        this.http = http;
        this.router = router;
        this.which = "none";
        this.deviceForm = fb.group({
            'deviceID': [''],
            'deviceName': ['']
        });
        this.api_url = api_url + "/admin/devices/add";
    }
    ContentComponent.prototype.addDevice = function (devForm) {
        var _this = this;
        var tmpValue = devForm.value;
        console.log("tmpVal", tmpValue);
        //submit data to server
        this.http.post(this.api_url, JSON.stringify(tmpValue))
            .subscribe(function (res) {
            var data = res.json();
            console.log(data);
            if (data.response.toLowerCase() === 'successful') {
                _this.deviceForm.reset();
                alert("Device successfully added");
                _this.which = 'none';
            }
        });
    };
    ContentComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ContentComponent.prototype, "which", void 0);
    ContentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-content',
            template: __webpack_require__("../../../../../src/app/components/content/content.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/content/content.component.css")]
        }),
        __param(3, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])("API_SERVER_URL")),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__services_activity_service__["a" /* Activity */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], String, __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]])
    ], ContentComponent);
    return ContentComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#wraper {\n    background-color:grey;\n    overflow:scroll;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\">\n    <app-header></app-header>\n          <!-- Page Content -->\n          <div id=\"page-content-wrapper\">\n              <div class=\"container-fluid\">\n                   <div class=\"row\">\n                      <div class=\"col-md-3 \">\n                        <app-left-sidebar (menuEvent)=\"menu($event)\"></app-left-sidebar>\n                      </div>\n                      <div class=\"col-md-6 \">\n                       <app-content [which] = 'menuContent'></app-content>\n                      </div>\n                      <div class=\"col-md-3 \">\n                          <div class=\"item text-center\">\n                            <app-right-sidebar></app-right-sidebar>\n                         </div>\n                      </div>\n                  </div>\n\n                  <!--footer-->\n                  <div class=\"row\">\n                    <div clas=\"col-md-12\">\n                        <app-footer></app-footer>\n                    </div>\n                  </div>\n              </div>\n          </div>\n          <!-- /#page-content-wrapper -->\n  \n      </div>\n      <!-- /#wrapper -->"

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DashboardComponent = (function () {
    function DashboardComponent() {
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent.prototype.menu = function (menuEvent) {
        this.menuContent = menuEvent;
        console.log("heello");
        console.log(menuEvent);
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/device-reg/device-reg.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/device-reg/device-reg.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Device registration section -->\n<form *ngIf=\"phase1\" (ngSubmit)=\"onSubmitDeviceReg()\">\n    \n    <div class='row'>\n        <div class=\"col-md-4\"></div>\n            <div class=\"col-md-4\" >\n                    <div class=\"header\">\n                            <h2 id=\"myModalLabel\" class=\"modal-title\">{{phaseTitle}}</h2>\n                    </div><br/>\n                    <div class=\"box\">\n                            <div class=\"form-group\">\n                                <label for=\"deviceID\">Device ID</label>\n                                <input [formControl] = \"deviceRegForm.controls['deviceID']\" class=\"form-control\"  placeholder=\"Device ID\" type=\"text\">\n              \n                            </div>\n                            <div class=\"form-group\">\n                                <label for=\"deviceName\">Device Name</label>\n                                <input [formControl] = \"deviceRegForm.controls['deviceName']\" class=\"form-control\"  placeholder=\"Device Name\" type=\"text\">\n                                \n                            </div>\n\n                            <div class=\"form-group\">\n                                    <label for=\"username\">User Name</label>\n                                    <input [formControl] = \"deviceRegForm.controls['username']\" class=\"form-control\"  placeholder=\"User Name\" type=\"text\">\n                            </div>\n\n                            <div class=\"form-group\">\n                                <button type=\"submit\" class=\"btn btn-danger form-control\">REGISTER DEVICE</button><br/><br/>\n                            </div>\n                        \n                    </div>\n                </div>\n\n                  <div class=\"col-md-4\"></div>\n    </div>\n</form>\n\n<div class=\"row\" *ngIf=\"phase2\">\n    <div class=\"col-md-3\"></div>\n    <div class=\"col-md-6\">\n        <div class=\"box\">\n            <p>Device Successfuly registered</p>\n            <a routerLink = \"/admin/dashboard\" class=\"btn btn-info\">Home</a>\n        </div>\n    </div>\n    <div class=\"col-md-3\"></div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/device-reg/device-reg.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceRegComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var DeviceRegComponent = (function () {
    function DeviceRegComponent(fb, http, api_url) {
        this.fb = fb;
        this.http = http;
        this.phase1 = true;
        this.phase2 = false;
        this.phaseTitle = "REGISTER DEVICE";
        this.api_url = api_url + "/user/device/register";
        //create the user registration form using form builder
        this.deviceRegForm = fb.group({
            //bio data
            "deviceID": [''],
            "deviceName": [''],
            "username": ['']
        });
    }
    DeviceRegComponent.prototype.nextForm = function (id) {
        switch (id) {
            case 'phase1':
                this.phase2 = false;
                this.phase1 = true;
                break;
            case 'phase2':
                this.phase1 = false;
                this.phase2 = true;
                break;
        }
    };
    DeviceRegComponent.prototype.dataIsValid = function (formVal) {
        return true;
    };
    DeviceRegComponent.prototype.onSubmitDeviceReg = function (form) {
        var _this = this;
        this.formValue = this.deviceRegForm.value;
        console.log(this.formValue);
        if (this.dataIsValid(this.formValue)) {
            //send registration details to the server
            this.http.post(this.api_url, JSON.stringify(this.formValue))
                .subscribe(function (res) {
                console.log("response from server:", res.json());
                //assumming the reg process went well
                if (res.json().response.toLowerCase() === 'successful')
                    _this.phaseTitle = "Registration Successful";
                else
                    _this.phaseTitle = 'Faild to register Device---Check your parameters';
            });
            this.deviceRegForm.reset();
            //activate phase2 of the reg process
            this.phase1 = false;
            this.phase2 = true;
        }
        else {
            this.phaseTitle = "Invalide Data";
        }
    };
    DeviceRegComponent.prototype.ngOnInit = function () {
    };
    DeviceRegComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-device-reg',
            template: __webpack_require__("../../../../../src/app/components/device-reg/device-reg.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/device-reg/device-reg.component.css")]
        }),
        __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])('API_SERVER_URL')),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], String])
    ], DeviceRegComponent);
    return DeviceRegComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "footer{\n    background-color:rgb(1, 1, 7);\n    position:fixed;\n    width:100%;\n    top:90%;\n    height:10%;\n    left:0;\n    border-style:solid;\n    border-color:red;\n    border-bottom:0; border-right:0;\n    border-left:0;\n}\n\n.custom-glyph {\n    font-size:2em;\n    \n}\n\n.glyph-container{\n    text-align:center;\n    color:white;\n    display:inline-block;\n    padding-left:10%;\n    padding-right:10%;\n    padding-top:0.5%;\n}\n\n.glyph-container:hover{\n    background-color:blue;\n    text-align:center;\n    color:white;\n    transition:1s;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer>\n    <div class=\"glyph-container\">\n        <span class=\"glyphicon glyphicon-cog custom-glyph\"></span>\n        <p>Settings</p>\n    </div>\n    \n                <div class=\"glyph-container\">\n                        <span class=\"glyphicon glyphicon-stats custom-glyph\"></span>\n                        <p>Pending Issues</p>\n                </div>\n                \n       \n        \n                <div class=\"glyph-container\">\n                        <span class=\"glyphicon glyphicon-hourglass custom-glyph\"></span>\n                        <p>Unresolved Pushes</p>\n                </div>\n                \n       \n        <div>\n                <p>Sp 5</p>\n        </div>\n        <div>\n                <p>Sp 1</p>\n        </div>\n    \n</footer>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/header/header.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "header{\n    position:fixed;\n    background-color:rgb(0, 0, 12);\n    width:100%;\n    top:0;\n    height:15%;\n    left:0;\n    border-style:solid;\n    border-color:red;\n    border-top:0; border-right:0;\n    border-left:0;\n}\n\n.custom-glyph {\n    font-size:3em;\n}\n\n.glyph-container{\n    \n    text-align:center;\n    color:white;\n    display:inline-block;\n    padding-left:5%;\n    padding-right:5%;\n    padding-top:0.5%;\n}\n\n/*.glyph-container{\n    text-align:center;\n    color:white;\n    display:inline-block;\n    padding-left:10%;\n    padding-right:10%;\n    padding-top:0.5%;\n} */\n\n.glyph-container:hover{\n    background-color:blue;\n    text-align:center;\n    color:white;\n    transition:1s;\n}\n\n.logo {\n    max-width:50%;\n    max-height:50%;\n    padding-right:10%;\n}\n\n.logo-container{\n    max-width:200px;\n    max-height:200px;\n    max-width:70%;\n    max-height:70%;\n    display:inline;\n    margin-right:1%;\n}\n\n.logo-container h2 {\n    font-size:1.5em;\n    color:white;\n    font-style:bold;\n    display:inline;\n    text-align:left;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<header>\n    \n        \n                <div class=\"logo-container\">\n                   <img src='../../assets/images/LOGO.png' class = 'logo' alt=\"Logo\">\n                   <h2>\n                        ADMIN DASHBOARD\n                    </h2>\n                </div>\n        \n        <div class=\"glyph-container\">\n                <span class=\"glyphicon glyphicon-phone-alt custom-glyph\"></span>\n                <p>Call Centers</p>\n        </div>\n                    \n                                <div class=\"glyph-container\" (click)=\"gotoMap()\">\n                                        <span class=\"glyphicon glyphicon-globe custom-glyph\"></span>\n                                        <p>Map</p>\n                                </div>\n                                \n                       \n                        \n                                <div class=\"glyph-container\">\n                                        <span class=\"glyphicon glyphicon-phone custom-glyph\"></span>\n                                        <p>Pending Pushes</p>\n                                </div>\n</header>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var HeaderComponent = (function () {
    function HeaderComponent(router, api_url) {
        this.router = router;
        this.api_url = api_url;
    }
    HeaderComponent.prototype.gotoMap = function () {
        //this.router.navigateByUrl(this.api_url+"/map");
        document.location.href = this.api_url + "/map";
    };
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-header',
            template: __webpack_require__("../../../../../src/app/components/header/header.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/header/header.component.css")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])('API_SERVER_URL')),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */], String])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/left-sidebar/left-sidebar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".menu {\n    width:15%;\n    height:78%;\n    background-color:black;\n    position:fixed;\n    top:15%;\n    left:0;\n}\n\n.menu ul {\n    height:100%;\n    padding-left:10%; padding-right:10%;\n    padding-bottom:15%; padding-top:8%;\n    list-style:none;\n    width:100%;\n    \n}\n\n.menu ul li {\n    border-style:solid;\n    border-top:0; \n    border-left:0; border-right:0;\n    border-color:red;\n    border-width:1px;\n    padding-top:3%; padding-bottom:6%;\n    padding-right:6%;\n    font-family:verdana,serif;\n    font-size:17px;\n}\n\n.menu ul li a:link {\n    text-decoration:none, !important;\n    color:white;\n}\n\n.menu ul li:hover {\n    background-color:rgb(245, 239, 239);\n    padding-top:6%; padding-bottom:6%;\n    padding-right:20%; padding-left:4%;\n    color:rgb(214, 81, 81);\n}\n\n.menu h3 {\n    color:white;\n    background-color:rgb(199, 30, 30);\n    padding:5%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/left-sidebar/left-sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"menu\">\n    <h3>\n        Admin Menu\n    </h3>\n  <ul>\n    <li>\n        <span class=\"glyphicon glyphicon-map-marker\"></span>\n      <a routerLink=\"/admin/dashboard\" (click)=\"handleClick('pushes')\">Pushes</a>\n    </li>\n\n    <li>\n        <span class=\"glyphicon glyphicon-user\"></span>\n      <a routerLink=\"/admin/dashboard\" (click)=\"handleClick('users')\">\n        Users</a>\n    </li>\n\n    <li>\n        <span class=\"glyphicon glyphicon-plus\"></span><span class=\"glyphicon glyphicon-user\"></span>\n        <a routerLink=\"/admin/dashboard\" (click) = \"handleClick('add user')\">Add User</a>\n    </li>\n\n    <li>\n        <span class=\"glyphicon glyphicon-hdd\"></span>\n      <a routerLink=\"/admin/dashboard\" (click) = \"handleClick('add device')\">Add Device</a>\n    </li>\n\n    <li>\n        <span class=\"glyphicon glyphicon-floppy-save\"></span>\n        <a routerLink=\"/admin/dashboard\" (click) = \"handleClick('register device')\">Register Device</a>\n    </li>\n\n    <li>\n        <span class=\"glyphicon glyphicon-phone\"></span>\n        <a routerLink=\"/admin/dashboard\" (click) = \"handleClick('devices')\">Devices</a>\n      </li>\n\n    <li>\n        <span class=\"glyphicon glyphicon-plus-sign\"></span><span class=\"glyphicon glyphicon-user\"></span>\n        <a routerLink=\"/admin/dashboard\" (click) = \"handleClick('add admin')\">Create Admin</a>\n    </li>\n\n    <li>\n        <span class=\"glyphicon glyphicon-list\"></span>\n        <a routerLink=\"/admin/dashboard\" (click) = \"handleClick('list admin')\">List Admin</a>\n    </li>\n  </ul>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/left-sidebar/left-sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeftSidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_activity_service__ = __webpack_require__("../../../../../src/app/services/activity.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var LeftSidebarComponent = (function () {
    function LeftSidebarComponent(http, api_url, activity) {
        this.http = http;
        this.activity = activity;
        this.menuEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.api_url = api_url;
    }
    LeftSidebarComponent.prototype.ngOnInit = function () {
    };
    LeftSidebarComponent.prototype.getData = function (data) {
        var _this = this;
        var tmpURL = '';
        if (data === 'devices' || data === 'pushes')
            tmpURL = "/admin/" + data;
        else
            tmpURL = "/" + data;
        this.http.request(this.api_url + tmpURL)
            .subscribe(function (res) {
            //update the activities pushes array
            //console.log(res);
            switch (data) {
                case 'pushes':
                    _this.activity.pushList = res.json().response;
                    console.log(_this.activity.pushList);
                    break;
                case "users":
                    _this.activity.userList = res.json().response;
                    _this.menuEvent.emit("users");
                    break;
                case "admins":
                    _this.activity.adminList = res.json().response;
                    _this.menuEvent.emit('list admin');
                    break;
                case 'devices':
                    _this.activity.deviceList = res.json().response;
                    console.log("dddd");
                    _this.menuEvent.emit("devices");
                    break;
            }
        });
    };
    LeftSidebarComponent.prototype.handleClick = function (which) {
        switch (which) {
            case "pushes":
                //fetch pushes from db
                this.getData('pushes');
                console.log("pussse");
                this.menuEvent.emit("pushes");
                break;
            case "users":
                //fetch users from db
                this.getData('users');
                break;
            case "devices":
                //fetch 
                this.getData('devices');
                break;
            case "add device":
                this.menuEvent.emit("add device");
                break;
            case "add user":
                this.menuEvent.emit("add user");
                break;
            case "register device":
                this.menuEvent.emit("register device");
                break;
            case "list admin":
                this.getData('admins');
                break;
            case "add admin":
                this.menuEvent.emit("add admin");
                break;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], LeftSidebarComponent.prototype, "menuEvent", void 0);
    LeftSidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-left-sidebar',
            template: __webpack_require__("../../../../../src/app/components/left-sidebar/left-sidebar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/left-sidebar/left-sidebar.component.css")]
        }),
        __param(1, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])("API_SERVER_URL")),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */], String, __WEBPACK_IMPORTED_MODULE_2__services_activity_service__["a" /* Activity */]])
    ], LeftSidebarComponent);
    return LeftSidebarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "  <div class=\"container-fluid\" *ngIf = \"!userService.loggedIn\">\n    <div class=\"row\">\n      <div class=\"col-sm-4 col-sm-offset-4 login\">\n      <h3 class=\"\">Admin Login</h3>\n       <form (ngSubmit)=\"onSubmit(loginForm)\" [formGroup] = \"loginForm\">\n          <div class=\"form-group\">\n              <label for=\"deviceName\">User Name</label>\n              <input [formControl] = \"loginForm.controls['username']\" class=\"form-control\"  placeholder=\"Admin User Name\" type=\"text\">\n          </div>\n             <div class=\"form-group\">\n                <label for=\"name-login\">Password</label>\n                <input  class=\"form-control\" [formControl] = \"loginForm.controls['password']\" placeholder=\"Password\" type=\"password\">\n            </div>\n             <div class=\"form-group\">\n                <div class=\"form-group\">\n                      <button type=\"submit\" class=\"btn btn-danger form-control\">Sign in</button>\n                  </div>\n            </div>\n          </form>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-sm-4 col-sm-offset-4 text-center ld\">\n       \n      </div>\n    </div>\n  </div>\n\n  <app-dashboard *ngIf=\"userService.loggedIn\" routerLink=\"/dashboard\"></app-dashboard>\n"

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_user_service__ = __webpack_require__("../../../../../src/app/services/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var LoginComponent = (function () {
    function LoginComponent(fb, http, api_url, userService, router) {
        this.fb = fb;
        this.http = http;
        this.userService = userService;
        this.router = router;
        this.api_url = api_url + "/admin/login";
        this.api_ur = api_url;
        this.API_URL = "http://localhost:4200";
        this.loginForm = fb.group({
            "username": [''],
            "password": ['']
        });
    }
    LoginComponent.prototype.onSubmit = function (formValue) {
        var _this = this;
        this.formValue = formValue.value;
        //send login details to the server
        this.http.post(this.api_url, JSON.stringify(this.formValue))
            .subscribe(function (res) {
            console.log("response from server:", res.json());
            if (res.json().response.toLowerCase() === "successful") {
                console.log("llll");
                _this.userService.loggedIn = true;
                _this.router.navigateByUrl('/admin/dashboard');
                //window.location.href=this.api_ur+"/admin/dashboard";
            }
            else {
                alert("invalid login details");
            }
        });
        this.loginForm.reset();
    };
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/components/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login/login.component.css")]
        }),
        __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])('API_SERVER_URL')),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], String, __WEBPACK_IMPORTED_MODULE_4__services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col-sm-4 col-sm-offset-4 login\">\n    <h3 class=\"\">Add New Admin</h3>\n     <form (ngSubmit)=\"onSubmit(regForm)\" [formGroup] = \"regForm\">\n        <div class=\"form-group\">\n            <label for=\"deviceName\">Admin User Name</label>\n            <input [formControl] = \"regForm.controls['username']\" class=\"form-control\"  placeholder=\"Admin User Name\" type=\"text\">\n        </div>\n        <div class=\"form-group\">\n          <label for=\"deviceName\">Admin User Name</label>\n          <input [formControl] = \"regForm.controls['email']\" class=\"form-control\"  placeholder=\"Email\" type=\"email\">\n        </div>\n           <div class=\"form-group\">\n              <label for=\"name-login\">Password</label>\n              <input  class=\"form-control\" [formControl] = \"regForm.controls['password']\" placeholder=\"Password\" type=\"password\">\n          </div>\n           <div class=\"form-group\">\n              <div class=\"form-group\">\n                    <button type=\"submit\" class=\"btn btn-danger form-control\">Add Admin</button>\n                </div>\n          </div>\n        </form>\n    </div>\n  </div>\n  <div class=\"row\">\n    <div class=\"col-sm-4 col-sm-offset-4 text-center ld\">\n     \n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_user_service__ = __webpack_require__("../../../../../src/app/services/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var RegisterComponent = (function () {
    function RegisterComponent(fb, http, api_url, userService) {
        this.fb = fb;
        this.http = http;
        this.userService = userService;
        this.api_url = api_url + "/admin/register";
        this.regForm = fb.group({
            "username": [''],
            "email": [''],
            "password": ['']
        });
    }
    RegisterComponent.prototype.onSubmit = function (formValue) {
        this.formValue = formValue.value;
        //send registration details to the server
        this.http.post(this.api_url, JSON.stringify(this.formValue))
            .subscribe(function (res) {
            console.log("response from server:", res.json());
            if (res.json().response.toLowerCase() === "successful") {
                console.log("llll");
                //window.location.href=this.API_URL+"/dashboard";
                alert("Admin registration successful");
            }
            else {
                alert("invalid registration details");
            }
        });
        this.regForm.reset();
    };
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-register',
            template: __webpack_require__("../../../../../src/app/components/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/register/register.component.css")]
        }),
        __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])('API_SERVER_URL')),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], String, __WEBPACK_IMPORTED_MODULE_3__services_user_service__["a" /* UserService */]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/right-sidebar/right-sidebar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".menu {\n    width:15%;\n    height:78%;\n    background-color:black;\n    position:fixed;\n    top:15%;\n    left:85%;\n}\n\n.menu ul {\n    height:100%;\n    padding:15%;\n    list-style:none;\n    \n}\n\n.menu ul li {\n    border-style:solid;\n    border-top:0; \n    border-left:0; border-right:0;\n    border-color:red;\n    border-width:1px;\n    padding-top:6%; padding-bottom:6%;\n    padding-right:6%;\n    font-family:verdana,serif;\n    font-size:17px;\n    color:white;\n}\n\n.menu h3 {\n    color:white;\n    background-color:rgb(199, 30, 30);\n    padding:5%;\n}\n\n\n.push-list{\n    list-style-type:none;\n}\n\n.push-item {\n    border-style:solid;\n    padding:1%;\n}\n\n.times {\n    width:20%;\n    height:20%;\n    background-color:rgb(228, 6, 6);\n    padding:10%;\n    border-radius:100%;\n    border-width:2%;\n    border-style:solid;\n    color:white;\n    \n}\n\n.push-text{\n    padding:5%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/right-sidebar/right-sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class = 'menu'>\n    <h3>\n        Resolved History\n    </h3>\n    <ul class=\"push-list\">\n        <li class=\"push-item\">\n          Resolved Push 1\n        </li>\n        <li>\n                Resolved Push 2\n        </li>\n        <li>\n                Resolved Push 3\n        </li>\n      </ul>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/right-sidebar/right-sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RightSidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng_socket_io__ = __webpack_require__("../../../../ng-socket-io/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_activity_service__ = __webpack_require__("../../../../../src/app/services/activity.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RightSidebarComponent = (function () {
    function RightSidebarComponent(activity, socket) {
        this.activity = activity;
        this.socket = socket;
        //make sure to change the hardcoded value theorix to current logged in user
        this.socket.on(activity.username, function (pushRes) {
            //now add this latest sent push to the pushes list
            console.log(pushRes);
        });
    }
    RightSidebarComponent.prototype.ngOnInit = function () {
    };
    RightSidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-right-sidebar',
            template: __webpack_require__("../../../../../src/app/components/right-sidebar/right-sidebar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/right-sidebar/right-sidebar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_activity_service__["a" /* Activity */], __WEBPACK_IMPORTED_MODULE_1_ng_socket_io__["Socket"]])
    ], RightSidebarComponent);
    return RightSidebarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/user-register/user-register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".container {\n    overflow-y:scroll;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/user-register/user-register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container register\">\n       <div class=\"header\">\n          <h2 id=\"myModalLabel\" class=\"modal-title\">{{phaseTitle}}</h2>\n      </div>\n       <form  [formGroup] = 'userRegForm' (ngSubmit)=\"onSubmitUserReg()\">\n              <div class=\"row\">\n                <div class=\"col-md-3\"></div>\n\n                  <div class=\"col-md-6\" *ngIf=\"phase1\">\n                      <div class=\"box\">\n                          <div class=\"form-group\">\n                              <label for=\"name-login\">Fullname</label>\n                              <input [formControl] = \"userRegForm.controls['fullname']\" class=\"form-control\"  placeholder=\"Enter your fullname\" type=\"text\">\n                          </div>\n                          <div class=\"form-group\">\n                              <label for=\"email-login\">Gender</label>\n                              <select class=\"form-control\" [formControl] = \"userRegForm.controls['gender']\">\n                                  <option disabled selected >Gender</option>\n                                  <option>Male</option>\n                                  <option>Female</option>\n                              </select>\n                          </div> \n                          <div class=\"form-group\">\n                              <label for=\"email-login\">State</label>\n                              <select class=\"form-control\" [formControl] = \"userRegForm.controls['state']\">\n                                  <option disabled selected style=\"display: none;\">Select State</option>\n                                  <option>Cross River</option>\n                              </select>\n                          </div>\n                          <div class=\"form-group\">\n                              <label for=\"username\">Choose Username</label> \n                              <input [formControl] = \"userRegForm.controls['username']\" class=\"form-control\"  placeholder=\"Enter your Unique Username. No spaces\" type=\"text\">\n                          </div>\n                      </div>\n                      <a routerLink=\"/register\" class=\"btn btn-danger form-control\" (click)=\"nextForm('phase2')\">Proceed</a>\n                  </div>\n\n                  <div class=\"col-md-6\" *ngIf=\"phase2\">\n                      <div class=\"box\">\n                              <div class=\"form-group\">\n                                  <label for=\"contact\">Mobile contact</label>\n                                  <input [formControl] = \"userRegForm.controls['phone']\" class=\"form-control\"  placeholder=\"Phone number\" type=\"text\">\n                \n                              </div>\n                              <div class=\"form-group\">\n                                  <label for=\"email\">Email</label>\n                                  <input [formControl] = \"userRegForm.controls['email']\" class=\"form-control\"  placeholder=\"Enter your Email address\" type=\"email\">\n                \n                              </div>\n                              <div class=\"form-group\">\n                                  <label for=\"password\">Password</label>\n                                  <input [formControl] = \"userRegForm.controls['password1']\" class=\"form-control\"  placeholder=\"Please enter your password\" type=\"password\">\n                              </div>\n                              <div class=\"form-group\">\n                                  <label for=\"password\">Confirm Password</label>\n                                  <input [formControl] = \"userRegForm.controls['password2']\" class=\"form-control\"  placeholder=\"Please confirm your password\" type=\"password\">\n                              </div>\n                              <div class=\"form-group\">\n                                  <a routerLink=\"/user/register\" (click)=\"nextForm('phase3')\" class=\"btn btn-danger form-control\">Next</a><br/><br/>\n                                  <a routerLink=\"/user/register\" class=\"btn btn-danger form-control\" (click)=\"nextForm('phase1')\">Back</a>\n\n                              </div>\n                          \n                      </div>\n                  </div>\n\n                  <div class=\"col-md-6\" *ngIf=\"phase4\">\n                      <div class=\"box\">\n                        <p>Full Name: {{formValue.fullname}}</p>\n                        <p>Gender: {{formValue.gender}}</p>\n                        <p>State: {{formValue.state}}</p>\n                        <p>Email Address: {{formValue.email}}</p>\n                        <p>Phone Number: {{formValue.phone}}</p>\n                        <p>User Name: {{formValue.username}}</p>\n                        <a routerLink=\"/admin/device/register\" class=\"btn btn-info\">Register a Device Now</a>\n                        <a routerLink = \"/admin/dashboard\" class=\"btn btn-info\">Home</a>\n                      </div>\n                  </div>\n                \n                  <div class=\"col-md-3\"></div>\n\n                </div>\n                <!-- /.row -->\n\n                  <!-- New phase section -->\n                  <div class='row' *ngIf=\"phase3\">\n        \n                        <div class=\"col-md-6\" >\n                                <div class=\"box\">\n                                        \n                                        <div class=\"form-group\">\n                                            <label for=\"houseAddress\">House Address</label>\n                                            <input [formControl] = \"userRegForm.controls['houseAddress']\" class=\"form-control\"  placeholder=\"Your House Address\" type=\"text\">\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label for=\"homePhone\">Home Phone</label>\n                                            <input [formControl] = \"userRegForm.controls['homePhone']\" class=\"form-control\"  placeholder=\"Please enter your Home Phone number\" type=\"text\">\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label for=\"alternatePhone\">Alternate Phone</label>\n                                            <input [formControl] = \"userRegForm.controls['alternatePhone']\" class=\"form-control\"  placeholder=\"Alternate Phone number\" type=\"text\">\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label for=\"landmark\">Land Mark</label>\n                                            <input [formControl] = \"userRegForm.controls['landmark']\" class=\"form-control\"  placeholder=\"Land Mark\" type=\"text\">\n                                        </div>\n                                    \n                                </div>\n                            </div>\n            \n                              <div class=\"col-md-6\">\n                                <div class=\"form-group\">\n                                    <label for=\"spousePhone\">Spouse Phone</label>\n                                    <input [formControl] = \"userRegForm.controls['spousePhone']\" class=\"form-control\"  placeholder=\"Your Spouse Phone number\" type=\"text\">\n                                </div>\n                                <div class=\"form-group\">\n                                    <label for=\"neighbourPhone\">Neighbour's Phone</label>\n                                    <input [formControl] = \"userRegForm.controls['neighbourPhone']\" class=\"form-control\"  placeholder=\"Your Neighbour's Phone number\" type=\"text\">\n                                </div>\n                                <div class=\"form-group\">\n                                    <label for=\"nearestBusStop\">Nearest Bus Stop</label>\n                                    <input [formControl] = \"userRegForm.controls['nearestBusStop']\" class=\"form-control\"  placeholder=\"Nearest Bus Stop\" type=\"text\">\n                                </div>\n                                <div class=\"form-group\">\n                                        <button type=\"submit\" class=\"btn btn-danger form-control\">CREATE ACCOUNT</button><br/><br/>\n                                        <a routerLink=\"/user/register\" class=\"btn btn-danger form-control\" (click)=\"nextForm('phase2')\">Back</a>\n                                    </div>\n                              </div>\n                </div>\n                  <!-- end phase -->\n          </form>\n        </div>\n\n     \n       <!-- /.container -->   \n\n\n"

/***/ }),

/***/ "../../../../../src/app/components/user-register/user-register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserRegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var UserRegisterComponent = (function () {
    function UserRegisterComponent(fb, http, api_url) {
        this.fb = fb;
        this.http = http;
        this.phase1 = true;
        this.phase2 = false;
        this.phase3 = false;
        this.phase4 = false;
        this.phaseTitle = "CREATE ACCOUNT NOW";
        this.api_url = api_url + "/user/register";
        //create the user registration form using form builder
        this.userRegForm = fb.group({
            //bio data
            "fullname": [''],
            "gender": [''],
            "state": [''],
            "username": [''],
            //contact info
            "houseAddress": [''],
            "phone": [''],
            "email": [''],
            "homePhone": [''],
            "landmark": [''],
            "alternatePhone": [''],
            "spousePhone": [''],
            "neighbourPhone": [''],
            "nearestBusStop": [''],
            //other info
            "password1": [''],
            "password2": [''],
            "password": ['']
        });
    }
    UserRegisterComponent.prototype.ngOnInit = function () {
    };
    UserRegisterComponent.prototype.nextForm = function (id) {
        switch (id) {
            case 'phase1':
                this.phase2 = false;
                this.phase1 = true;
                break;
            case 'phase2':
                this.phase1 = false;
                this.phase2 = true;
                this.phase3 = false;
                break;
            case 'phase3':
                this.phase2 = false;
                this.phase3 = true;
                break;
            case 'phase4':
                this.phase3 = false;
                this.phase4 = true;
                this.phaseTitle = "Register Device";
                break;
        }
    };
    UserRegisterComponent.prototype.onSubmitUserReg = function (form) {
        var _this = this;
        this.formValue = this.userRegForm.value;
        console.log(this.formValue);
        if (this.formValue.password1 === this.formValue.password2) {
            this.formValue.password = this.formValue.password1;
            //send registration details to the server
            this.http.post(this.api_url, JSON.stringify(this.formValue))
                .subscribe(function (res) {
                console.log("response from server:", res.json());
                //assumming the reg process went well
                _this.phaseTitle = "Registration Successful";
            });
            this.userRegForm.reset();
            //activate phase4 of the reg process
            this.phase2 = false;
            this.phase3 = false;
            this.phase4 = true;
        }
        else {
            this.phaseTitle = "Password Does not match";
        }
    };
    UserRegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-user-register',
            template: __webpack_require__("../../../../../src/app/components/user-register/user-register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/user-register/user-register.component.css")]
        }),
        __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])('API_SERVER_URL')),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], String])
    ], UserRegisterComponent);
    return UserRegisterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/services/activity.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Activity; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Activity = (function () {
    function Activity() {
        this.pushList = [];
        // this.username = userService.getUserName();
    }
    Activity.prototype.addPush = function (push) {
        this.pushList.push(push);
        console.log("push added");
    };
    Activity = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], Activity);
    return Activity;
}());



/***/ }),

/***/ "../../../../../src/app/services/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { User } from "../models/user.model.ts";

var UserService = (function () {
    function UserService() {
        this.username = '';
        this.loggedIn = false;
        this.devices = [];
    }
    UserService.prototype.setUserName = function (name) {
        this.username = name;
    };
    UserService.prototype.getUserName = function () {
        return this.username;
    };
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map