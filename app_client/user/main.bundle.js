webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n\n<nav class=\"navbar container-fluid \" role=\"navigation\">\n    <div class=\"navbar-header\">\n      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\"\n      data-target=\"#example-navbar-collapse\">\n        <span class=\"sr-only\">Toggle navigation</span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n      </button>\n      <a class=\"navbar-brand\" routerLink=\"/\"><img src=\"assets/images/LOGO.png\"></a>\n    </div>\n    <div class=\"collapse navbar-right  navbar-collapse\" id=\"example-navbar-collapse\">\n      <ul class=\"nav navbar-nav\">\n        <li class=\"active\"><a routerLink=\"/user/home\">HOME</a></li>\n        <li><a routerLink=\"/user/login\" *ngIf=\"!userService.loggedIn\">LOGIN</a></li>\n        <li *ngIf=\"userService.loggedIn\"><a routerLink=\"/login\"  (click) = \"logout()\">LOGOUT</a></li>\n      </ul>\n    </div>\n  </nav>\n<app-dashboard *ngIf=\"userService.loggedIn\"></app-dashboard>\n<router-outlet *ngIf=\"!userService.loggedIn\"></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_service__ = __webpack_require__("../../../../../src/app/services/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(userService) {
        this.userService = userService;
        this.title = 'app';
        console.log(userService);
    }
    AppComponent.prototype.logout = function () {
        if (this.userService.loggedIn)
            this.userService.loggedIn = false;
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_register_register_component__ = __webpack_require__("../../../../../src/app/components/register/register.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_sidebar_sidebar_component__ = __webpack_require__("../../../../../src/app/components/sidebar/sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_home_home_component__ = __webpack_require__("../../../../../src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_nav_bar_nav_bar_component__ = __webpack_require__("../../../../../src/app/components/nav-bar/nav-bar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_device_reg_device_reg_component__ = __webpack_require__("../../../../../src/app/components/device-reg/device-reg.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_user_service__ = __webpack_require__("../../../../../src/app/services/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var server_url = 'http://45.55.167.142:3001';








//services

var routes = [
    {
        path: "", redirectTo: 'user/home', pathMatch: "full"
    },
    {
        path: "user", redirectTo: "user/home", pathMatch: 'full'
    },
    {
        path: "user/home", component: __WEBPACK_IMPORTED_MODULE_10__components_home_home_component__["a" /* HomeComponent */]
    },
    {
        path: "user/login", component: __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__["a" /* LoginComponent */]
    },
    {
        path: "user/register", component: __WEBPACK_IMPORTED_MODULE_6__components_register_register_component__["a" /* RegisterComponent */]
    },
    {
        path: "user/device/register", component: __WEBPACK_IMPORTED_MODULE_12__components_device_reg_device_reg_component__["a" /* DeviceRegComponent */]
    },
    {
        path: "user/dashboard", component: __WEBPACK_IMPORTED_MODULE_8__components_dashboard_dashboard_component__["a" /* DashboardComponent */]
    }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_register_register_component__["a" /* RegisterComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_sidebar_sidebar_component__["a" /* SidebarComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_nav_bar_nav_bar_component__["a" /* NavBarComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_device_reg_device_reg_component__["a" /* DeviceRegComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* RouterModule */].forRoot(routes)
            ],
            providers: [
                { provide: 'API_SERVER_URL', useValue: server_url },
                __WEBPACK_IMPORTED_MODULE_13__services_user_service__["a" /* UserService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"wrapper\" ng-class=\"class\">\n\n  <app-sidebar></app-sidebar>  \n  <!--<userpageheader></userpageheader> -->\n          <!-- Page Content -->\n          <div id=\"page-content-wrapper\">\n              <div class=\"container-fluid\">\n                <div class=\"row\">\n                    <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12 \">\n                      <div class=\"info-box blue-bg\">\n                         <i class=\"glyphicon glyphicon-hdd\"></i>\n                        <div class=\"count\">{{userService.devices.length}}</div>\n                        <div class=\"title\">Number of device</div>           \n                      </div><!--/.info-box-->     \n                    </div><!--/.col-->\n  \n                    <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">\n                      <div class=\"info-box red-bg\">\n                         <i class=\"glyphicon glyphicon-lamp\"></i>\n                        <div class=\"count\">{{userService.isSubscribed}}</div>\n                        <div class=\"title\">Subscribed</div>           \n                      </div><!--/.info-box-->     \n                    </div><!--/.col-->\n  \n                    <div class=\"col-lg-4 col-md-4 col-sm-12 col-xs-12\">\n                      <div class=\"info-box blue-bg\">\n                         <i class=\"icon_bag\"></i>\n                        <div class=\"count\"></div>\n                        <div class=\"title\">additional value</div>           \n                      </div><!--/.info-box-->     \n                    </div><!--/.col-->\n                </div>\n              </div>\n          </div>\n          <!-- /#page-content-wrapper -->\n  \n  </div>\n      <!-- /#wrapper -->\n      "

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_user_service__ = __webpack_require__("../../../../../src/app/services/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardComponent = (function () {
    function DashboardComponent(userService) {
        this.userService = userService;
        console.log(userService.loggedIn);
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-dashboard',
            template: __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_user_service__["a" /* UserService */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/device-reg/device-reg.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/device-reg/device-reg.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Device registration section -->\n<form *ngIf=\"phase1\" (ngSubmit)=\"onSubmitDeviceReg()\">\n    \n    <div class='row'>\n        <div class=\"col-md-4\"></div>\n            <div class=\"col-md-4\" >\n                    <div class=\"header\">\n                            <h2 id=\"myModalLabel\" class=\"modal-title\">{{phaseTitle}}</h2>\n                    </div><br/>\n                    <div class=\"box\">\n                            <div class=\"form-group\">\n                                <label for=\"deviceID\">Device ID</label>\n                                <input [formControl] = \"deviceRegForm.controls['deviceID']\" class=\"form-control\"  placeholder=\"Device ID\" type=\"text\">\n              \n                            </div>\n                            <div class=\"form-group\">\n                                <label for=\"deviceName\">Device Name</label>\n                                <input [formControl] = \"deviceRegForm.controls['deviceName']\" class=\"form-control\"  placeholder=\"Device Name\" type=\"text\">\n                                \n                            </div>\n\n                            <div class=\"form-group\">\n                                    <label for=\"username\">User Name</label>\n                                    <input [formControl] = \"deviceRegForm.controls['username']\" class=\"form-control\"  placeholder=\"User Name\" type=\"text\">\n                            </div>\n\n                            <div class=\"form-group\">\n                                <button type=\"submit\" class=\"btn btn-danger form-control\">REGISTER DEVICE</button><br/><br/>\n                            </div>\n                        \n                    </div>\n                </div>\n\n                  <div class=\"col-md-4\"></div>\n    </div>\n</form>\n\n<div class=\"row\" *ngIf=\"phase2\">\n    <div class=\"col-md-3\"></div>\n    <div class=\"col-md-6\">\n        <div class=\"box\">\n            <p>Device Successfuly registered</p>\n            <a routerLink = \"/home\" class=\"btn btn-info\">Home</a>\n        </div>\n    </div>\n    <div class=\"col-md-3\"></div>\n</div>\n\n"

/***/ }),

/***/ "../../../../../src/app/components/device-reg/device-reg.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceRegComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var DeviceRegComponent = (function () {
    function DeviceRegComponent(fb, http, api_url) {
        this.fb = fb;
        this.http = http;
        this.phase1 = true;
        this.phase2 = false;
        this.phaseTitle = "REGISTER DEVICE";
        this.api_url = api_url + "/user/device/register";
        //create the user registration form using form builder
        this.deviceRegForm = fb.group({
            //bio data
            "deviceID": [''],
            "deviceName": [''],
            "username": ['']
        });
    }
    DeviceRegComponent.prototype.nextForm = function (id) {
        switch (id) {
            case 'phase1':
                this.phase2 = false;
                this.phase1 = true;
                break;
            case 'phase2':
                this.phase1 = false;
                this.phase2 = true;
                break;
        }
    };
    DeviceRegComponent.prototype.dataIsValid = function (formVal) {
        return true;
    };
    DeviceRegComponent.prototype.onSubmitDeviceReg = function (form) {
        var _this = this;
        this.formValue = this.deviceRegForm.value;
        console.log(this.formValue);
        if (this.dataIsValid(this.formValue)) {
            //send registration details to the server
            this.http.post(this.api_url, JSON.stringify(this.formValue))
                .subscribe(function (res) {
                console.log("response from server:", res.json());
                //assumming the reg process went well
                _this.phaseTitle = "Registration Successful";
            });
            this.deviceRegForm.reset();
            //activate phase2 of the reg process
            this.phase1 = false;
            this.phase2 = true;
        }
        else {
            this.phaseTitle = "Invalide Data";
        }
    };
    DeviceRegComponent.prototype.ngOnInit = function () {
    };
    DeviceRegComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-device-reg',
            template: __webpack_require__("../../../../../src/app/components/device-reg/device-reg.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/device-reg/device-reg.component.css")]
        }),
        __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Inject */])('API_SERVER_URL')),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], String])
    ], DeviceRegComponent);
    return DeviceRegComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"bg img-responsive\"></div> -->\n<div class=\"jumbotron\" id=\"landing-jumbotron\">\n  <div class=\"container\">\n    <div class=\"jumbotron-text\">\n      <h1>Hot Button</h1>\n      <p>Get your own device now</p>\n      <br>\n      <p>\n        <a class=\"btn btn-danger btn-lg\" role=\"button\">\n          Get a device\n        </a> &nbsp; &nbsp;\n        <a routerLink=\"/user/register\" class=\"btn btn-danger btn-lg\" role=\"button\">\n          Create account\n        </a>\n      </p>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-home',
            template: __webpack_require__("../../../../../src/app/components/home/home.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "  <div class=\"container-fluid con1\">\n    <div class=\"row\">\n      <div class=\"col-sm-4 col-sm-offset-4 login\">\n      <h3 class=\"\">User Login</h3>\n       <form (ngSubmit)=\"onSubmit(loginForm)\" [formGroup] = \"loginForm\">\n          <div class=\"form-group\">\n              <label for=\"deviceName\">User Name</label>\n              <input [formControl] = \"loginForm.controls['username']\" class=\"form-control\"  placeholder=\"User Name\" type=\"text\">\n          </div>\n             <div class=\"form-group\">\n                <label for=\"name-login\">Password</label>\n                <input  class=\"form-control\" [formControl] = \"loginForm.controls['password']\" placeholder=\"Enter your Password\" type=\"password\">\n            </div>\n             <div class=\"form-group\">\n                <div class=\"form-group\">\n                      <button type=\"submit\" class=\"btn btn-danger form-control\">Sign in</button>\n                  </div>\n            </div>\n          </form>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-sm-4 col-sm-offset-4 text-center ld\">\n       \n      </div>\n    </div>\n  </div>\n"

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_user_service__ = __webpack_require__("../../../../../src/app/services/user.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var LoginComponent = (function () {
    function LoginComponent(fb, http, api_url, userService, router) {
        this.fb = fb;
        this.http = http;
        this.userService = userService;
        this.router = router;
        this.api_url = api_url + "/user/login";
        this.API_URL = "http://localhost:4200";
        this.loginForm = fb.group({
            "username": [''],
            "password": ['']
        });
    }
    LoginComponent.prototype.onSubmit = function (formValue) {
        var _this = this;
        this.formValue = formValue.value;
        //send login details to the server
        this.http.post(this.api_url, JSON.stringify(this.formValue))
            .subscribe(function (res) {
            console.log("response from server:", res.json());
            if (res.json().response.toLowerCase() === "successful") {
                console.log("llll");
                _this.userService.loggedIn = true;
                _this.router.navigateByUrl('/user/dashboard');
                //window.location.href=this.API_URL+"/dashboard";
            }
            else {
                alert("invalid login details");
            }
        });
        this.loginForm.reset();
    };
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/components/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login/login.component.css")]
        }),
        __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Inject */])('API_SERVER_URL')),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], String, __WEBPACK_IMPORTED_MODULE_4__services_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/nav-bar/nav-bar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/nav-bar/nav-bar.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  nav-bar works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/nav-bar/nav-bar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavBarComponent = (function () {
    function NavBarComponent() {
    }
    NavBarComponent.prototype.ngOnInit = function () {
    };
    NavBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-nav-bar',
            template: __webpack_require__("../../../../../src/app/components/nav-bar/nav-bar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/nav-bar/nav-bar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NavBarComponent);
    return NavBarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container register\">\n       <div class=\"header\">\n          <h2 id=\"myModalLabel\" class=\"modal-title\">{{phaseTitle}}</h2>\n      </div>\n       <form  [formGroup] = 'userRegForm' (ngSubmit)=\"onSubmitUserReg()\">\n              <div class=\"row\">\n                <div class=\"col-md-3\"></div>\n\n                  <div class=\"col-md-6\" *ngIf=\"phase1\">\n                      <div class=\"box\">\n                          <div class=\"form-group\">\n                              <label for=\"name-login\">Fullname</label>\n                              <input [formControl] = \"userRegForm.controls['fullname']\" class=\"form-control\"  placeholder=\"Enter your fullname\" type=\"text\">\n                          </div>\n                          <div class=\"form-group\">\n                              <label for=\"email-login\">Gender</label>\n                              <select class=\"form-control\" [formControl] = \"userRegForm.controls['gender']\">\n                                  <option disabled selected >Gender</option>\n                                  <option>Male</option>\n                                  <option>Female</option>\n                              </select>\n                          </div> \n                          <div class=\"form-group\">\n                              <label for=\"email-login\">State</label>\n                              <select class=\"form-control\" [formControl] = \"userRegForm.controls['state']\">\n                                  <option disabled selected style=\"display: none;\">Select State</option>\n                                  <option>Cross River</option>\n                              </select>\n                          </div>\n                          <div class=\"form-group\">\n                              <label for=\"username\">Choose Username</label> \n                              <input [formControl] = \"userRegForm.controls['username']\" class=\"form-control\"  placeholder=\"Enter your Unique Username. No spaces\" type=\"text\">\n                          </div>\n                      </div>\n                      <a routerLink=\"/register\" class=\"btn btn-danger form-control\" (click)=\"nextForm('phase2')\">Proceed</a>\n                  </div>\n\n                  <div class=\"col-md-6\" *ngIf=\"phase2\">\n                      <div class=\"box\">\n                              <div class=\"form-group\">\n                                  <label for=\"contact\">Mobile contact</label>\n                                  <input [formControl] = \"userRegForm.controls['phone']\" class=\"form-control\"  placeholder=\"Phone number\" type=\"text\">\n                \n                              </div>\n                              <div class=\"form-group\">\n                                  <label for=\"email\">Email</label>\n                                  <input [formControl] = \"userRegForm.controls['email']\" class=\"form-control\"  placeholder=\"Enter your Email address\" type=\"email\">\n                \n                              </div>\n                              <div class=\"form-group\">\n                                  <label for=\"password\">Password</label>\n                                  <input [formControl] = \"userRegForm.controls['password1']\" class=\"form-control\"  placeholder=\"Please enter your password\" type=\"password\">\n                              </div>\n                              <div class=\"form-group\">\n                                  <label for=\"password\">Confirm Password</label>\n                                  <input [formControl] = \"userRegForm.controls['password2']\" class=\"form-control\"  placeholder=\"Please confirm your password\" type=\"password\">\n                              </div>\n                              <div class=\"form-group\">\n                                  <a routerLink=\"/user/register\" (click)=\"nextForm('phase3')\" class=\"btn btn-danger form-control\">Next</a><br/><br/>\n                                  <a routerLink=\"/user/register\" class=\"btn btn-danger form-control\" (click)=\"nextForm('phase1')\">Back</a>\n\n                              </div>\n                          \n                      </div>\n                  </div>\n\n                  <div class=\"col-md-6\" *ngIf=\"phase4\">\n                      <div class=\"box\">\n                        <p>Full Name: {{formValue.fullname}}</p>\n                        <p>Gender: {{formValue.gender}}</p>\n                        <p>State: {{formValue.state}}</p>\n                        <p>Email Address: {{formValue.email}}</p>\n                        <p>Phone Number: {{formValue.phone}}</p>\n                        <p>User Name: {{formValue.username}}</p>\n                        <a routerLink=\"/user/device/register\" class=\"btn btn-info\">Register a Device Now</a>\n                        <a routerLink = \"/\" class=\"btn btn-info\">Home</a>\n                      </div>\n                  </div>\n                \n                  <div class=\"col-md-3\"></div>\n\n                </div>\n                <!-- /.row -->\n\n                  <!-- New phase section -->\n                  <div class='row' *ngIf=\"phase3\">\n        \n                        <div class=\"col-md-6\" >\n                                <div class=\"box\">\n                                        \n                                        <div class=\"form-group\">\n                                            <label for=\"houseAddress\">House Address</label>\n                                            <input [formControl] = \"userRegForm.controls['houseAddress']\" class=\"form-control\"  placeholder=\"Your House Address\" type=\"text\">\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label for=\"homePhone\">Home Phone</label>\n                                            <input [formControl] = \"userRegForm.controls['homePhone']\" class=\"form-control\"  placeholder=\"Please enter your Home Phone number\" type=\"text\">\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label for=\"alternatePhone\">Alternate Phone</label>\n                                            <input [formControl] = \"userRegForm.controls['alternatePhone']\" class=\"form-control\"  placeholder=\"Alternate Phone number\" type=\"text\">\n                                        </div>\n                                        <div class=\"form-group\">\n                                            <label for=\"landmark\">Land Mark</label>\n                                            <input [formControl] = \"userRegForm.controls['landmark']\" class=\"form-control\"  placeholder=\"Land Mark\" type=\"text\">\n                                        </div>\n                                    \n                                </div>\n                            </div>\n            \n                              <div class=\"col-md-6\">\n                                <div class=\"form-group\">\n                                    <label for=\"spousePhone\">Spouse Phone</label>\n                                    <input [formControl] = \"userRegForm.controls['spousePhone']\" class=\"form-control\"  placeholder=\"Your Spouse Phone number\" type=\"text\">\n                                </div>\n                                <div class=\"form-group\">\n                                    <label for=\"neighbourPhone\">Neighbour's Phone</label>\n                                    <input [formControl] = \"userRegForm.controls['neighbourPhone']\" class=\"form-control\"  placeholder=\"Your Neighbour's Phone number\" type=\"text\">\n                                </div>\n                                <div class=\"form-group\">\n                                    <label for=\"nearestBusStop\">Nearest Bus Stop</label>\n                                    <input [formControl] = \"userRegForm.controls['nearestBusStop']\" class=\"form-control\"  placeholder=\"Nearest Bus Stop\" type=\"text\">\n                                </div>\n                                <div class=\"form-group\">\n                                        <button type=\"submit\" class=\"btn btn-danger form-control\">CREATE ACCOUNT</button><br/><br/>\n                                        <a routerLink=\"/user/register\" class=\"btn btn-danger form-control\" (click)=\"nextForm('phase2')\">Back</a>\n                                    </div>\n                              </div>\n                </div>\n                  <!-- end phase -->\n          </form>\n        </div>\n\n     \n       <!-- /.container -->   \n\n\n"

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var RegisterComponent = (function () {
    function RegisterComponent(fb, http, api_url) {
        this.fb = fb;
        this.http = http;
        this.phase1 = true;
        this.phase2 = false;
        this.phase3 = false;
        this.phase4 = false;
        this.phaseTitle = "CREATE ACCOUNT NOW";
        this.api_url = api_url + "/user/register";
        //create the user registration form using form builder
        this.userRegForm = fb.group({
            //bio data
            "fullname": [''],
            "gender": [''],
            "state": [''],
            "username": [''],
            //contact info
            "houseAddress": [''],
            "phone": [''],
            "email": [''],
            "homePhone": [''],
            "landmark": [''],
            "alternatePhone": [''],
            "spousePhone": [''],
            "neighbourPhone": [''],
            "nearestBusStop": [''],
            //other info
            "password1": [''],
            "password2": [''],
            "password": ['']
        });
    }
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent.prototype.nextForm = function (id) {
        switch (id) {
            case 'phase1':
                this.phase2 = false;
                this.phase1 = true;
                break;
            case 'phase2':
                this.phase1 = false;
                this.phase2 = true;
                this.phase3 = false;
                break;
            case 'phase3':
                this.phase2 = false;
                this.phase3 = true;
                break;
            case 'phase4':
                this.phase3 = false;
                this.phase4 = true;
                this.phaseTitle = "Register Device";
                break;
        }
    };
    RegisterComponent.prototype.onSubmitUserReg = function (form) {
        var _this = this;
        this.formValue = this.userRegForm.value;
        console.log(this.formValue);
        if (this.formValue.password1 === this.formValue.password2) {
            this.formValue.password = this.formValue.password1;
            //send registration details to the server
            this.http.post(this.api_url, JSON.stringify(this.formValue))
                .subscribe(function (res) {
                console.log("response from server:", res.json());
                //assumming the reg process went well
                _this.phaseTitle = "Registration Successful";
            });
            this.userRegForm.reset();
            //activate phase4 of the reg process
            this.phase2 = false;
            this.phase3 = false;
            this.phase4 = true;
        }
        else {
            this.phaseTitle = "Password Does not match";
        }
    };
    RegisterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-register',
            template: __webpack_require__("../../../../../src/app/components/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/register/register.component.css")]
        }),
        __param(2, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["z" /* Inject */])('API_SERVER_URL')),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */], String])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/sidebar/sidebar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".side-bar{\n    background-color:#3b5998;;\n    color:white;\n    position:fixed;\n    z-index:1;\n    top:0;\n    left:0;\n    width:0; /* 18% */\n    height:100%;\n    overflow-x:hidden;\n    opacity:0.8;\n    transition:0.5s;\n  }\n  \n  .side-bar a{\n    text-decoration:none;\n    color:white;\n    padding:15px 5px;\n    display:block;\n  \n    /*on mouse leave */\n    transition:2s;\n  }\n  \n  .side-bar ul a:hover{\n    background-color:#f4f4f4;\n    color:#000;\n    /*width:100%;\n    height:20px; */\n    font-size:25px;\n    text-align:center;\n    padding:10px 2px;\n    border-style:solid;\n    border-bottom:1px;\n    border-top: 0px;\n    border-left:0px; border-right:0px;\n    border-bottom-left-radius:20px;\n    transition:0.7s; \n  }\n  \n  .side-bar ul{\n    display:block;\n    list-style:none;\n    padding:10px 14px;\n    margin-top:80px;\n  }\n  \n  .side-bar ul li {\n    font-size:20px;\n    border-style:solid;\n    border-bottom:2;\n    border-top:0px;\n    border-left:0px;\n    border-right:0px;\n    border-bottom-left-radius:20px;\n  }\n  \n  #close-menu{\n    float:right;\n    padding:5px 5px;\n    color:white;\n    font-size:30px;\n    font-style:bold;\n  }\n  \n  #close-menu:hover {\n    background-color:#ccc;\n  }", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"side-bar\" id=\"side-menu\">\n  <a href=\"#\" id=\"close-menu\" (click)=\"closeSideMenu()\">\n      &times;\n  </a>\n  <ul id = \"leftMenu\">\n    <li><a href=\"#\">Widthdraw</a></li>\n    <li><a href=\"#\">Transfer</a></li>\n    <li><a href=\"#\">Deposit</a></li>\n    <li><a href=\"#\">Check Balance</a></li>\n    <li><a href=\"#\">Invoice</a></li>\n    <li><a href=\"#\">Settings</a></li>\n  </ul>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/sidebar/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidebarComponent = (function () {
    function SidebarComponent() {
    }
    SidebarComponent.prototype.ngOnInit = function () {
    };
    SidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-sidebar',
            template: __webpack_require__("../../../../../src/app/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/sidebar/sidebar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/services/user.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import { User } from "../models/user.model.ts";

var UserService = (function () {
    function UserService() {
        this.username = '';
        this.loggedIn = false;
        this.devices = [];
    }
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map