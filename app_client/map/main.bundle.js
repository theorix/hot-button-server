webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "agm-map {\n    height:600px;\n    width:1320px;\n    position:absolute;\n    left:40;\n}\n\nagm-marker{\n    margin-left:250px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n\n<agm-map [longitude] = \"lng\" [latitude] = \"lat\"  [zoomControl]=\"true\" [disableDefaultUI]=\"false\">\n  <agm-marker \n\n  *ngFor=\"let m of markers; let i = index\"\n  [latitude] = 'm.lat'\n  [longitude] = \"m.lng\"\n  (markerClick)=\"handleClick(m,i)\"\n  [visible] = \"true\"\n  \n  >\n\n  <agm-info-window>\n    <strong>{{m.label}}<br/>\n      {{m.lat}},{{m.lng}}\n    </strong>\n  </agm-info-window>\n\n  </agm-marker>\n\n   \n\n  <app-update-comp \n  [mapAPI2] = \"mappAPI\" \n  >\n</app-update-comp>\n</agm-map>\n\n<app-socket-component (pushEvent) = \"setCoordinates($event)\"></app-socket-component>\n\n\n\n\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__agm_core_directives_marker__ = __webpack_require__("../../../../@agm/core/directives/marker.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng_socket_io__ = __webpack_require__("../../../../ng-socket-io/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng_socket_io__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = (function () {
    /*
     markers: Array<marker> = [
      {
        lat: 51.673858,
        lng: 7.815982,
        label: 'A',
        draggable: true
      },
      {
        lat: 51.373858,
        lng: 7.215982,
        label: 'B',
        draggable: false
      },
      {
        lat: 51.723858,
        lng: 7.895982,
        label: 'C',
        draggable: true
      }
    ] */
    function AppComponent(mapAPI, markerManager, mapMarker, socket) {
        this.mapAPI = mapAPI;
        this.markerManager = markerManager;
        this.mapMarker = mapMarker;
        this.socket = socket;
        this.title = 'Google map';
        this.lat = 4.9928367;
        this.lng = 8.3441894;
        this.infoWindow = true;
        //assume that this user is called
        this.username = "theorix";
        this.audioElement = document.createElement("audio");
        this.audioElement.setAttribute("src", "/assets/siren.mp3");
        this.mappAPI = this.mapAPI;
        console.log("marker man", this.markerManager);
        this.markers = [{ lat: this.lat, lng: this.lng, draggable: true }];
    }
    AppComponent.prototype.setCoordinates = function (coord) {
        console.log(coord);
        //serializ the data
        var jsonData = JSON.parse(coord);
        var coordinates = jsonData.pushDetails;
        this.lat = Number(coordinates.lat);
        this.lng = Number(coordinates.lng);
        //remove the old marker-----comment out this line to enable multiple markers
        //this.markers.splice(0,this.markers.length);
        //add the new marker
        this.markers.push({ lat: this.lat, lng: this.lng, label: coordinates.user, draggable: true });
        //(this.mapMarker as AgmMarker).setPosition({lat:this.lat,lng:this.lng});
        //this.markerManager.updateMarkerPosition(this.mapMarker);
        // console.dir(AgmMarker);
        console.log("coordinates set successfully");
        //play sound
        console.log(this.markers);
        this.audioElement.play();
    };
    AppComponent.prototype.handleClick = function (label, i) {
        console.log("Location:", this.lat, ",", this.lng);
        /*
         When the marker is finally dragged to the appropriate position, the details of the marker/push
         is then sent to th server as push sent:
        */
        this.socket.emit("push sent", { lat: this.lat, lng: this.lng, agent: this.username, rescueTeam: "team 1" }); //beware, this coord is suppose to be the coordinate of the  marker clicked
    };
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent.prototype.toNum = function (num) {
        return +num;
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__agm_core__["c" /* GoogleMapsAPIWrapper */], __WEBPACK_IMPORTED_MODULE_1__agm_core__["d" /* MarkerManager */],
            __WEBPACK_IMPORTED_MODULE_2__agm_core_directives_marker__["a" /* AgmMarker */], __WEBPACK_IMPORTED_MODULE_3_ng_socket_io__["Socket"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng_socket_io__ = __webpack_require__("../../../../ng-socket-io/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng_socket_io__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_socket_component_socket_component_component__ = __webpack_require__("../../../../../src/app/components/socket-component/socket-component.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_update_comp_update_comp_component__ = __webpack_require__("../../../../../src/app/components/update-comp/update-comp.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var config = { url: 'http://45.55.167.142:3001', options: {} };
//const config: SocketIoConfig = { url: 'http://localhost:3001', options: {} };



var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_5__components_socket_component_socket_component_component__["a" /* SocketComponentComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_update_comp_update_comp_component__["a" /* UpdateCompComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__agm_core__["a" /* AgmCoreModule */].forRoot({
                    apiKey: "AIzaSyAwfGDp5V06qcck8n5BXGMO3If3THQSRqw"
                }),
                __WEBPACK_IMPORTED_MODULE_3_ng_socket_io__["SocketIoModule"].forRoot(config)
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_2__agm_core__["c" /* GoogleMapsAPIWrapper */], __WEBPACK_IMPORTED_MODULE_2__agm_core__["d" /* MarkerManager */], __WEBPACK_IMPORTED_MODULE_2__agm_core__["b" /* AgmMarker */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/components/socket-component/socket-component.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/socket-component/socket-component.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n Map Loading...\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/socket-component/socket-component.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocketComponentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng_socket_io__ = __webpack_require__("../../../../ng-socket-io/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng_socket_io___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng_socket_io__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SocketComponentComponent = (function () {
    function SocketComponentComponent(socket) {
        this.socket = socket;
        this.pushEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        //this.pushEvent = new EventEmitter();
        this.sendMessage("Socket connection received Successfully");
        //console.log("hh");
        this.getMessage();
    }
    SocketComponentComponent.prototype.sendMessage = function (msg) {
        this.socket.emit("message", msg);
    };
    SocketComponentComponent.prototype.getMessage = function () {
        //return this.socket;
        // .fromEvent("message")
        // .map( data => data );
        this.socket.on("message", function (msg) { console.log(msg); });
        this.socket.on("push", (function (pushEvent) {
            return function (push) {
                pushEvent.emit(push);
                // console.log(this.socket);
                console.log(push);
            };
        }(this.pushEvent)));
    };
    SocketComponentComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], SocketComponentComponent.prototype, "pushEvent", void 0);
    SocketComponentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-socket-component',
            template: __webpack_require__("../../../../../src/app/components/socket-component/socket-component.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/socket-component/socket-component.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ng_socket_io__["Socket"]])
    ], SocketComponentComponent);
    return SocketComponentComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/update-comp/update-comp.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/update-comp/update-comp.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  update-comp works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/update-comp/update-comp.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateCompComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__agm_core__ = __webpack_require__("../../../../@agm/core/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UpdateCompComponent = (function () {
    function UpdateCompComponent() {
    }
    UpdateCompComponent.prototype.ngOnInit = function () {
        this.mapAPI2.setCenter({ lat: 8.9095, lng: 5.3232 });
        //console.log(this.mapAPI2.getNativeMap());
        /*this.mapAPI2.getNativeMap().then(function(map){
          console.log(map);
        }) */
        //console.log(google);
    };
    UpdateCompComponent.prototype.markerObj = function (marker) {
        console.log("nothing to show");
        console.log(marker);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__agm_core__["c" /* GoogleMapsAPIWrapper */])
    ], UpdateCompComponent.prototype, "mapAPI2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], UpdateCompComponent.prototype, "latitude", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], UpdateCompComponent.prototype, "longitude", void 0);
    UpdateCompComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-update-comp',
            template: __webpack_require__("../../../../../src/app/components/update-comp/update-comp.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/update-comp/update-comp.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UpdateCompComponent);
    return UpdateCompComponent;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map