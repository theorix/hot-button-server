var http = require("http");
var parse = require("url").parse;
var join = require("path").join;
var fs = require("fs");

function StaticContext(webRoot) {
  this.webRoot = webRoot;
  this.contexts = [];   //the context list
  this.nonContexts = []; // the context to ignore
  this.currentCtx = '';   //current context
  let that = this; // a reference to this object

  this.staticFile = function staticFile(request,response) {
    var url = parse(request.url); console.log("req:",request);

    var path;
    var tmp = url.pathname.split('.');  //check for directory or file without extension
    if(tmp.length === 1) path = join(that.webRoot,url.pathname,"index.html");
    else path = join(that.webRoot,url.pathname);
     console.log("tmp:",tmp);
    var stream = fs.createReadStream(path);
    stream.pipe(response);
  };

  this.setWebRoot = function setWebRoot(newRoot) {
  that.webRoot = newRoot;
 }

 this.addContext = function addContext(ctx,type) {
   if(typeof ctx === 'string') {
     if(ctx[0] !== '/') ctx = '/'+ctx;
     if(type && type === 'non')
      that.nonContexts.push(ctx);
     else if(!type) that.contexts.push(ctx);
   }
 };

 this.setContext = function setContext(ctxArray,type) {
   if(typeof ctxArray === 'object' && typeof ctxArray.length !== 'undefined')
    ctxArray.forEach(function(ctx){
      if(type && type === 'non')
        that.addContext(ctx,type);
      else if(!type) that.addContext(ctx);
    });
 };


 this.isContext = function isContext(route,type) {
   let isCtx = false;
   let ctx = [];

   if(type && type === 'non') ctx = that.nonContexts;
   else if(!type) ctx = that.contexts;

   ctx.forEach(function(r){ console.log("r",r,"route",route);
     if(route === r || route === r+'/') isCtx = true;
   }); console.log("iscontext",isCtx);
   return isCtx;
 }

 this.setCurrentCtx = function(ctx) {
    that.currentCtx = ctx.split('/')[1];
  }

 this.middleware = function middleware(req,res,next) {
   //let that = this;
   if(req.method === 'GET') {       console.log("uRls:",req.method);
     if(that.isContext(req.url))
     {
       that.setCurrentCtx(req.url);
       //console.log('TTPM',tmpRoot);
       res.sendfile(join(that.webRoot,req.url,'index.html'));
     }
     else {
       //console.log("TMP:",tmpRoot);
       let tmpURL = req.url.split('/');
       let sitePath = tmpURL[1];
       let found = false;
       for(let i = 0; i < that.contexts.length; i++) {  //console.log("ctx:",that.contexts[i],"sitePath:",sitePath);
         if(sitePath === that.contexts[i].split('/')[1]  && !that.isContext(req.url,'non')) {
           res.sendfile(join(that.webRoot,that.contexts[i].split('/')[1],'index.html'));
           that.setCurrentCtx(that.contexts[i]);
           found = true;
           break;
         }
       }
       if(!found) {
         if(!that.isContext(req.url,'non'))
           res.sendfile(join(that.webRoot,that.currentCtx,req.url));
         else next();
       }
     }
   }
   else next();
 }

}

module.exports = StaticContext;
