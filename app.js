var app = require("express")();
var express = require("express");
var http = require("http").Server(app);
var http2 = require("http");
var io = require("socket.io")(http);
var path = require("path");
var StaticContext = require("./staticContext");

//models
let userModel = require("./models/user");
let adminModel = require("./models/admin");
let deviceModel = require("./models/device");
let pushesModel = require("./models/pushes");
require("./models/db");

//enable or disable debug mode
let debug_mode = true;

function headers(req,res,next) {
  res.setHeader("content-type","application/json");
  res.setHeader("Access-Control-Allow-Origin","*");
  next();
}

function report_error(errorMsg) {
  return `{"response":"Failed", "message":"${errorMsg}"}`;
}

function send_response(msg) {
  return `{"response":"Successful", "message":"${msg}"}`;
}

function debug_print(msg) {
  if(debug_mode) console.log(msg);
}

function list(req,res,type) { debug_print("list called");
  let data = '', model;
  if(type === 'pushes') model = pushesModel.Pushes;
  else if(type === 'users') model = userModel.User;
  else if(type === 'admin') model = adminModel.Admin;
  else model = deviceModel.Device;

  model.find().then(function(msg){
    try {
      res.end('{"response":'+JSON.stringify(msg)+'}');
    }
    catch(e) {
      res.end(repot_error(e.message));
    }
  }).catch(function(e){
        res.end(report_error(e.message));
  })
}

function login(req,res,type){
  let data = '';
  req.on("data",function(chunk) {
    data += chunk;
  });
  req.on("end",function(){
    try { console.log("try");
      userData = JSON.parse(data);
      let model;
      if(type === "user") model = userModel.User;
      else model = adminModel.Admin;
      model.find({username:userData.username, password:userData.password})
      .then(function(msg){
        if(JSON.stringify(msg) !== '[]') {
          debug_print(type,userData.username,"logged in success");
          //TODO: set and return session variables
          res.end(send_response("user is valid"));
        }
        else {
          res.end(report_error(type+" "+userData.username+" does not exist"));
        }
      }).catch(function(e){
        res.end(report_error(e.message));
      })
    }
    catch(e) {
      res.end(report_error(e.message));
    }

  })
}

function register(req,res,type) {
  let data = '';
  req.on("data",function(chunk){
    data += chunk;
  });

  req.on("end",function(){
    debug_print(data);
    //parse the json file and then create a user model
    try {
      let jsonObj = JSON.parse(data), model;
      if(type === 'user') model = userModel.createUser;
      else model = adminModel.createAdmin;
      model(jsonObj)
      .save().then(function() {
        res.end(send_response(type+" registeration Successful"));
      })
      .catch(function(e) {
        res.end(report_error(e.message));
      })
    }
    catch(e) {
      res.end(report_error(e.message));
    }

  });
}



//custom static file module
let staticCtx = new StaticContext(path.join(__dirname,'app_client'));
//let userWebRoot = new StaticModule(path.join(__dirname,'app_client'));
//let adminWebRoot = new StaticModule(path.join(__dirname,'app_client'));
let tmpRoot1 = 'user';
let tmpRoot2 = 'admin';
let tmpRoot3 = 'map';
tmpRoot = '';

function setVal(vars) {
   tmpRoot = vars.split('/')[1];
 }

 const GETroutes = ['/user','/admin','/map'];
 const APIroutes = ['/admin/pushes','/admins','/users','/admin/devices'];
 const userRoutes = ['/user/home','/user/register','/user/login','/user/dashboard','/user/device/regiser'];
 const adminRoutes = ['/admin/login','/admin/register','/admin/dashboard'];
 staticCtx.setContext(GETroutes);
 staticCtx.setContext(APIroutes,'non');

app.use(staticCtx.middleware);
//app.use(routeMiddleWare);


app.get("/user",function(req,res){

});

app.get("/admin",function(req,res){

});

app.get("/map",function(req,res){
  //res.sendfile('app_client/map/index.html');
});

app.use(headers);

//handle push requests
app.post("/",function(req,res){
  var data = "";
  req.on("data",function(chunk) {
    data += chunk;
  });
  req.on("end",function(){
    //check whether every parameter given matches with db
    let pushData, device, deviceOwner, pushResponse;
    try {
      pushData = JSON.parse(data);
      //check for the required fields
      if(!pushData.deviceID || !pushData.lat || !pushData.lng || !pushData.time)
        res.end(report_error("All Fields are required"));
      //check the for a valid deviceID

      deviceModel.Device.find({"deviceID":pushData.deviceID})
      .then(function(d){ console.log(typeof JSON.stringify(d));
        if(JSON.stringify(d) === '[]' ) {
          res.end(report_error("invalid device ID"));
        }
        else { //console.log(d);
          let dev = d;
          //fetch details of this device owner
          let deviceOwner;
          userModel.User.find({"username":dev[0].username})
          .then(function(u){
            deviceOwner = u;
            device = dev[0];

            //now that we have the device, it's owner and push details, we send them to client
            pushResponse = {device:device, deviceOwner:deviceOwner, pushDetails:pushData};
            //send push to clients
            io.sockets.emit("push",JSON.stringify(pushResponse));
            debug_print(JSON.stringify(pushResponse));

            //store push in the data Database
            pushesModel.createPush(pushData)
            .save().then(function(msg){
              debug_print("Push stored:",msg);
            }).catch(function(e) {
              console.log(e);
            });
            //send respone to caller
            res.end(send_response("push received"));

          }).catch(function(e){
            console.log(e.message);
          });
        }


      }).catch(function(e){
        console.log(e.message);
      });
    }
    catch(e) {
      console.log(e.message);
    }


    //res.end("Push Successful");

  });

});

//user registration
app.post("/user/register",function(req,res){
  register(req,res,"user");
});

app.post('/admin/register',function(req,res) {
  register(req,res,"admin");
});

//device registration
app.post("/user/device/register",function(req, res){
  let data = '';
  req.on("data",function(chunk){
    data += chunk;
  });

  req.on("end",function(){
    try {
        let deviceData = JSON.parse(data);
      //check whether the device is legit
      if(!deviceData.deviceID || !deviceData.deviceName || !deviceData.username)
        res.end(report_error("All Fields Required"));
      else {  //TODO: check the given user whether or not it exists
        deviceModel.Device.update({deviceID:deviceData.deviceID},
          {$set:{username:deviceData.username}},
        {multi:true},function(error,numAffected) {
          if(error) res.end(report_error(error.message));
          else {
            res.end(send_response("Device registration successful"));
          }
        });
      }
    }
    catch(e) {
      res.end(report_error(e.message));
    }
  });
});

//login
app.post("/user/login",function(req,res){
  login(req,res,"user");
});

app.post("/admin/login",function(req,res){
  login(req,res,"admin");
});

//temporary url for adding devices to db
app.post("/admin/devices/add",function(req,res) {
  let data = '';
  req.on("data",function(chunk){
    data += chunk;
  });
  req.on("end",function(){
    try {
      let device = JSON.parse(data);
      deviceModel.createDevice(device)
      .save().then(function(msg){
        console.log(msg);
        res.end(send_response("device added"));
      }).catch(function(e){
        console.log(e.message);
        res.end(report_error(e.message));
      });
    } catch(e) {
      console.log(e.message);
      res.end(report_error(e.message));
    }
  });
});

//get all pushes
app.get("/admin/pushes",function(req,res){
  list(req,res,"pushes");
});

app.get("/users",function(req,res){ console.log("users called")
  list(req,res,"users");
});

app.get("/admin/devices",function(req,res){
  list(req,res,"devices");
});

app.get("/admins",function(req,res){
  list(req,res,"admin");
})


//handle client connections
io.on("connection",function(socket){
  console.log("connected");
  socket.on("-message",function(msg){
    console.log(msg);
    console.log("pushing data to client");
    socket.emit("push",'{"lng":"3.8998","lat":"3.9605"}');
  });

  //listen for agent signal
  socket.on("push sent",function(pushData) {
    console.log("push data:",pushData);

    //.....1) save this sent push to db
    //.....2) propagate this info to the admin dashboard
    console.log(pushData.agent);
    io.sockets.emit(pushData.agent,pushData);
  });

  //listen for push resolve
  socket.on("resolve",function(pushData){
    try { console.log(typeof pushData)
      //let pData = JSON.parse(pushData);
      //TODO: send this push data to the right socket only
      io.sockets.emit("unresolved",pushData);
    }catch(e){
      console.log("unresolved:",e.message);
    }
  });

});

http.listen(3001,function(){
  console.log("server listening on port 3001");
});
