var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

var supportSchema = mongoose.Schema(
    {
	  useer: {type: Schema.Types.ObjectId, ref: 'User' },
	  subject: {type: String, required: true, unique: true},
	  body: {type: String, required: true, unique: true},
	  comments: [{
	    person: {type: Schema.Types.ObjectId, ref: 'User' },
	    comment: {type: String, required: true, unique: true},
	    created_at: {type: Date, default: Date.now}
	  }],
	  created_at: {type: Date, default: Date.now},
	  read: {type: Boolean,default: false},
	  resolved:{type: Boolean,default: false},
	}

)
var Support = mongoose.model('Support', deviceSchema);

module.exports = Support;