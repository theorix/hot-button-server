var mongoose = require('mongoose');
//var jwt = require('jsonwebtoken');
var Schema = mongoose.Schema;


var pushesSchema = Schema(
    {
        deviceID: {type: String, required: true},
        lat: {type: String, required: true},
        lng: {type: String, required: true},
        time: {type: String, required: true},
        status: {type: String, default:"unresolved"}
    }
);

var Pushes = mongoose.model('Pushes', pushesSchema);

function createPush(pushObj) {
  let push = new Pushes({
    deviceID: pushObj.deviceID,
    lat: pushObj.lat,
    lng: pushObj.lng,
    time: pushObj.time
  });

  return push;
}

exports.Pushes = Pushes;
exports.createPush = createPush;
