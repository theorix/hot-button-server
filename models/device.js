var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

var deviceSchema = mongoose.Schema(
    {
        deviceID: {type: String, required: true, unique: true},
        deviceName: {type: String, required: true, unique: true},
        createdAt: {type: Date, default: Date.now},
        activated: {type: Boolean, default: false},
        username: {type: String, default: "-"},
        houseGPSLoc: {type: String, default: "-"}
    }
)
var Device = mongoose.model('Device', deviceSchema);

function createDevice(deviceObj) {
  let device = new Device({
    deviceID: deviceObj.deviceID,
    deviceName: deviceObj.deviceName,
    username: deviceObj.username,
    houseGPSLoc: deviceObj.houseGPSLoc
  });
  return device;
}

exports.Device = Device;
exports.createDevice = createDevice;
