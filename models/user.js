var mongoose = require('mongoose');
//var jwt = require('jsonwebtoken');
var Schema = mongoose.Schema;


var userSchema = Schema(
    {
        fullname: {type: String, required: true},
        gender: {type: String, required: true},
        state: {type: String, required: true},
        email: {type: String, required: true, unique: true},
        username: {type: String, required: true, unique: true},
        phone: {type: String, required: true},
        alternatePhone: {type: String, default: "-"},
        homePhone: {type: String, default: "-"},
        spousePhone: {type: String, default: "-"},
        neighbourPhone: {type: String, default: "-"},
        houseAddress: {type: String, default: "-"},
        nearestBusStop: String,
        landmark: String,
        password: {type: String, required: true},
        isSubscribed: {type: Boolean, default: false},
        createdAt: {type: Date, default: Date.now}
    }
)

/*
userSchema.methods.generateJwt = function() {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);
    return jwt.sign({
        _id: this._id,
        email: this.email,
        username: this.username,
        exp: parseInt(expiry.getTime() / 1000),
    }, 'thisIsSecret' );
}; */

var User = mongoose.model('User', userSchema);

//function to create a user model
function createUser(userObj) {

  //use only the required fields for now
  let userModel = new User({
    fullname: userObj.fullname,
    gender: userObj.gender,
    state: userObj.state,
    email: userObj.email,
    username: userObj.username,
    phone: userObj.phone,
    password: userObj.password,
    houseAddress:userObj.houseAddress,
    homePhone:userObj.homePhone,
    landmark:userObj.landmark,
    alternatePhone:userObj.alternatePhone,
    spousePhone:userObj.spousePhone,
    neighbourPhone:userObj.neighbourPhone,
    nearestBusStop:userObj.nearestBusStop
  });

  return userModel;
}
 
//export both the model and it's create method
exports.User = User;
exports.createUser = createUser;
