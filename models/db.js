var mongoose = require("mongoose");

// Database host and database name
var host = 'localhost';
var dbName = 'hot_button_test';

// MongoDB connect string(URL string)
var db_url = 'mongodb://' + host + '/' + dbName;

if (process.env.HEROKU) {
  var host = '';
  var dbName = '';
  var username = '';
  var password = '';
  // MongoDB connect string(URL string)
  db_url = 'mongodb://' + username + ':' + password + '@' + host + '/' + dbName;
} else if (process.env.PROD) {
  var host = 'localhost';
  var dbName = 'hot_button';
  var username = 'serveradmin';
  var password = 'secureANDpassed';
  // MongoDB connect string(URL string)
  db_url = 'mongodb://' + username + ':' + password + '@' + host + '/' + dbName;
}

mongoose.connect(db_url, {
    useMongoClient: true
  },
  function(err) {
    if (err) {
      console.error('' + err);
    } else {
      console.log('Database connected!');
    }
  }
);
