var mongoose = require('mongoose');
//var jwt = require('jsonwebtoken');

var adminSchema = mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});



/*
adminSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);
  return jwt.sign({
    _id: this._id,
    email: this.email,
    username: this.username,
    exp: parseInt(expiry.getTime() / 1000),
  }, 'thisIsSecret');
}; */

var Admin = mongoose.model('Admin', adminSchema);

function createAdmin(adminObj) {
  let adminModel = new Admin({
    username:adminObj.username,
    email: adminObj.email,
    password:adminObj.password
  });
  return adminModel;
}

exports.createAdmin = createAdmin;
exports.Admin = Admin;
